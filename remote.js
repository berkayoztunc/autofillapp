const {
  ipcRenderer
} = require('electron');


var query = 0;
ipcRenderer.on('find-form-remote', (event, arg) => {
  var input = document.forms[arg].querySelectorAll('input');
  query = arg;
  var names = Array.from(input).map((r) => {
    return r.name;
  }, {});
  ipcRenderer.send('form', names)
})
ipcRenderer.on('send-data', (event, arg) => {
  document.forms[query].querySelectorAll('input').forEach((item) => {
    console.log(item);
    console.log(arg[item.name]);

    item.value = arg[item.name]
  });
  setTimeout(() => {
    var e = new CustomEvent("submit");  
    document.forms[query].submit()
    //document.forms[query].dispatchEvent(e)
    
  }, 20)
})