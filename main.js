// Modules to control application life and create native browser window
const {
  app,
  BrowserWindow
} = require('electron')
const path = require('path')
const {
  ipcMain
} = require('electron');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 800,
    x: 0,
    y: 0,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    },
    icon: path.join(__dirname, 'logo.png')
  })
  mainWindow.webContents.openDevTools()
  mainWindow.loadFile('index.html')
  mainWindow.on('closed', function () {
    mainWindow = null
  })
  ipcMain.on('url-update', (event, arg) => {
    let child = new BrowserWindow({
      parent: mainWindow,
      icon: path.join(__dirname, 'png/logo.png'),
      width: 200,
      height: 200,
      webPreferences: {
        preload: path.join(__dirname, 'remote.js')
      }
    })
    child.loadURL(arg)
    child.show()
    ipcMain.on('find-form',(event,arg)=>{
      if(child){
        console.log(arg);
        child.webContents.send('find-form-remote', arg);
      }
    })
    ipcMain.on('send-data',(event,arg)=>{
      if(child){
        console.log(arg);
        child.webContents.send('send-data', arg);
        setTimeout(()=>{
          child.destroy();
        },7500)
      }
    })
    child.on('closed', () => {
      child = null
    })
  })
  ipcMain.on('form', (event, arg) => {
    mainWindow.webContents.send('form', arg);
  });
  

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.