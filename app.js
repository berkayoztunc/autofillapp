var app = new Vue({
    el: '#app',

    data() {
        return {
            url: 'https://berkayoztunc.com/iletisim',
            query: '0',
            formSelector: [],
            sender: {},
            datas: [],
            header: [],
            range: {
                start: 0,
                end: 1
            },
            order: 0,
            sitetest: false,
            formtest: false,
            start: false,
            run: false,

            timer: '00:00',
            timerObj: null,
            senderTimerOnj: null,
            senderTimerOnjOpener: null,
        }
    },

    mounted() {
        let obj = localStorage.getItem('storage')
        this.setter(obj)

        setTimeout(() => {
            console.clear()
            console.log(`%c Hazır`, 'color:#03fc17')
            console.log(`%c İşleme başlamak için formun bulunduğu ekranun URl adresini url alanına girin. Sonrasında sistem üzerinde aralık belirletin. Bu işlemlerden sonra form ile uygun olan csv'yi yükleyin ve başlat tuşuna basın`, 'color:#ccc729')
            console.log(obj);

        }, 1000)

    },
    methods: {
        emptyList() {
            this.datas = [];
            this.order = 0;
            this.savaState()
        },
        setter(obj) {
            obj = JSON.parse(obj);
            if (obj != null) {
                this.order = obj.order
                this.datas = obj.datas
                this.range = obj.range
                this.url = obj.url
                this.header = obj.header
                this.query = obj.query
                this.formSelector = obj.formSelector
                this.sender = obj.sender
                this.sitetest = obj.sitetest
                this.formtest = obj.formtest
                this.start = obj.start
                this.run = obj.run
            }
        },
        demo() {
            let obj = '{"order":1,"datas":[{"name":"wberkay7","email":"test@test.com","phone":"123456123","status":"beklemede..."}],"range":{"start":0,"end":1},"url":"https://berkayoztunc.com/iletisim","query":"0","formSelector":["name","email","phone"],"sender":{"name":"berkayw","email":"test@test.com","phone":"123456123","status":"Gönderildi Sat Jan 11 2020 20:24:06 GMT+0300 (GMT+03:00)"},"header":["name","email","phone","status"],"sitetest":true,"formtest":true,"start":false,"run":true}';
            this.setter(obj);
            let intro = introJs();
            intro.oncomplete(function () {
                location.reload();
            });
            intro.onexit(function () {
                location.reload();
            });
            setTimeout(() => {
                intro.refresh();
                intro.start();

            }, 50)


        },
        startTimer(duration) {
            time = duration * 60;
            tmp = time;
            this.timerObj = setInterval(()=> {
                var c = tmp--,
                    m = (c / 60) >> 0,
                    s = (c - m * 60).toFixed(0) + '';
                this.timer = ('' + m + ':' + (s.length > 1 ? '' : '0') + s)
                tmp != 0 || (tmp = time);
            }, 1000);

        },
        stop() {
            this.senderTimerOnj != null ? clearTimeout(this.senderTimerOnj) : null;
            this.senderTimerOnjOpener != null ? clearTimeout(this.senderTimerOnjOpener) : null;
            this.timerObj != null ? clearInterval(this.timerObj) : null;
            this.timer = '00:00'
            this.start = false;
            this.savaState()
        },
        savaState() {
            let obj = {
                order: this.order,
                datas: this.datas,
                range: this.range,
                url: this.url,
                query: this.query,
                formSelector: this.formSelector,
                sender: this.sender,
                header: this.header,
                sitetest: this.sitetest,
                formtest: this.formtest,
                start: this.start,
                run: this.run,
            }
            localStorage.setItem('storage', JSON.stringify(obj));
        },
        senderer() {
            this.start = true
            this.timerObj != null ? clearInterval(this.timerObj) : null;
            if (this.datas[this.order]) {
                let data = this.datas[this.order]
                this.sender = data
                let random = chance.floating({
                    min: this.range.start,
                    max: this.range.end
                });
                this.startTimer(random);
                data.status = 'Hazırlandı +'
                this.senderTimerOnjOpener = setTimeout(() => {
                    this.initial()
                    this.getForm();
                }, 1000 * 60 * this.range.start)

                this.senderTimerOnj = setTimeout(() => {
                    this.send(true);
                    this.order = this.order + 1
                }, 1000 * 60 * random);
            } else {
                this.stop()
            }
        },
        getForm() {
            console.log(`%c Form istendi ${this.query}`, 'color:#f78502')
            window.formFind(this.query)
            this.formtest = true;
        },
        initial() {
            console.log(`%c url girildi ${this.url}`, 'color:#f78502')
            window.sendUrl(this.url)
            this.sitetest = true;
        },
        send(init) {
            console.log(`%c Bilgi gönderildi`, 'color:#03fc17')
            let date = Date(Date.now())
            this.sender.status = 'Gönderildi ' + date.toString();
            window.sendData(this.sender)
            if (init) {
                this.senderer();
            }
            this.savaState()
        },
        manuelSend() {
            let data = this.datas[this.order]
            this.order = this.order + 1
            this.sender = data
            this.send(false);
            this.stop();
            console.log(`%c Sistem manuel gönderim yapıldı lütfen yeniden başlatın`, 'color:#03fc17')

        },
        fillSend(item) {
            this.sender = item
            this.send(false);
        },
        loadTextFromFile(ev) {

            const file = ev.target.files[0];
            const reader = new FileReader();
            reader.readAsText(file);

            reader.onload = e => {
                const data = reader.result;
                const options = {
                    header: true,
                    complete: (result) => {
                        this.header = Object.keys(result.data[0])
                        this.header.push('status');
                        let value = _.map(result.data, (item) => {
                            item.status = 'beklemede...'
                            return item
                        })
                        this.datas = [...this.datas, ...value]
                        this.run = true
                        ev.target.value = null
                    }
                };
                Papa.parse(data.toString(), options);
            };
        }
    }
})