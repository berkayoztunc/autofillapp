const {
  ipcRenderer
} = require('electron');
ipcRenderer.on('form', (event, arg) => {
  console.log(arg);
  document.getElementById('app').__vue__.formSelector = arg
});

window.sendUrl = function(url){
  ipcRenderer.send('url-update', url);
}
window.sendData = function(data){
  ipcRenderer.send('send-data', data);
}

window.formFind = function(data){
  ipcRenderer.send('find-form', data);
}
